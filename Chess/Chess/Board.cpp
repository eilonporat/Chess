#include "Board.h"

#define START_BOARD "rbnkqnbrpppppppp################################PPPPPPPPRBNQKNBR1"

//c'tor
Board::Board(string whiteName, string blackName) : _white(true, whiteName), _black(false, blackName)
{
	string startBoard = START_BOARD;
	strcpy_s(this->_chars, START_BOARD);	// initalize the char array
	strcpy_s(this->_previousChars, START_BOARD);	// initalize the char array
}
//d'tor
Board::~Board()
{}

char* Board::getChars()
{
	return this->_chars;
}
/*
Input:
	NON
Output:
	function changing the turn to the next player
*/
void Board::nextTurn()
{
	if (this->_chars[PLAYER_TURN] == '1')
	{
		this->_chars[PLAYER_TURN] = '0';
	}
	else
	{
		this->_chars[PLAYER_TURN] = '1';
	}
	strcpy_s(this->_previousChars, this->_chars);
}
/*
Input:
	the destnation and the source (like 'e4e2')
Output:
	change location of the right soldiers
*/
void Board::makeMove(string msg)
{
	int src = 0, dst = 0;
	Helpers::messageToLocation(msg, src, dst);
	
	// change soldier location of white/black player
	if (this->_chars[PLAYER_TURN] == '0')
	{
		this->_white.changeSoldierLocation(src, dst);
	}
	else
	{
		this->_black.changeSoldierLocation(src, dst);
	}
	// update board data
	this->_chars[dst] = this->_chars[src];
	this->_chars[src] = '#';
}
/*
Input:
	the destnation and the source (like 'e4e2')
Output:
	returns the status (for instance: 0: ok, 1: made chess, 2: there is no soldier at src, etc..)
	makes the move
	if move is illegal, rewind
*/
int Board::getStatus(string msg)
{
	int src = 0, dst = 0;

	// equal indexes check
	if (this->isEqualIndexes(msg))
	{
		return DST_SRC_ARE_SAME;
	}

	// check that indexes are legal
	if (!this->isLegalIndex(msg))
	{
		return NON_BOARD_INDEXES;
	}
	
	// check that we don't have any tool in dst
	if (this->dstHasTool(msg, this->_chars[PLAYER_TURN]))
	{
		return NO_DST;
	}
	
	// check that we do have tool in src
	if (!this->checkSoldierIn(this->_chars[PLAYER_TURN], msg))
	{
		return NO_SRC;
	}

	// check that the tool can move to dst
	if (this->_chars[PLAYER_TURN] == '1')	// black
	{
		if (!this->_black.canMoveTo(msg, this->getChars()))
		{
			return TOOL_CANT_MOVE;
		}
	}
	else // white
	{
		if (!this->_white.canMoveTo(msg, this->getChars()))
		{
			return TOOL_CANT_MOVE;
		}
	}

	// check if we eat another player piece
	if (this->_chars[PLAYER_TURN] == '1')	// black
	{
		if (this->dstHasTool(msg, '0'))
		{
			Helpers::messageToLocation(msg, src, dst);
			this->_white.deleteSoldier(dst);
		}
	}
	else  // white
	{
		if (this->dstHasTool(msg, '1'))
		{
			Helpers::messageToLocation(msg, src, dst);
			this->_black.deleteSoldier(dst);
		}
	}

	// make the move
	this->makeMove(msg);
	
	// check chess
	if (this->_chars[PLAYER_TURN] == '1')	// black
	{
		
		if (this->_white.madeChess(this->getChars()))
		{
			this->rewind(msg);
			return MADE_CHESS_ON_PLAYER;
		}
		else if (this->_black.madeChess(this->getChars()))
		{
			return MADE_CHESS_OK;
		}
	}
	else  // white
	{
		if (this->_black.madeChess(this->getChars()))
		{
			this->rewind(msg);
			return MADE_CHESS_ON_PLAYER;
		}
		else if (this->_white.madeChess(this->getChars()))
		{
			return MADE_CHESS_OK;
		}
	}
	
	return CODE_OK;
}

/*
input:
	message from graphics
output:
	returns the soldier's location to the last turn location
*/
void Board::rewind(string msg)
{
	int src = 0, dst = 0;
	string s = "";
	// make the opposite move (rewind last move)
	Helpers::messageToLocation(msg, src, dst);
	s += Helpers::locationToMessage(dst);
	s += Helpers::locationToMessage(src);
	this->makeMove(s);
	// update char array
	strcpy_s(this->_chars, this->_previousChars);

}


bool Board::dstHasTool(string msg, char turn)
{
	int src = 0, dst = 0;
	char c = ' ';
	Helpers::messageToLocation(msg, src, dst);

	if (turn == '0')
	{
		c = this->_chars[dst];
		return isUpper(c);
	}
	if (turn == '1')
	{
		c = this->_chars[dst];
		return isLower(c);
	}
}

/*
Input:
	NON
Output:
	prints the board
*/
void Board::printChars()
{
	int i = 0, j = 0;
	//moving through the string, printing its chars
	for (i = 0; i < COL; i++)// print board 8*8
	{
		for (j = 0; j < ROW; j++)
		{
			cout << this->_chars[i*ROW + j] << "  ";
		}
		cout << endl;
	}
}
/*
Input:
	the destnation and the source (like 'e4e2')
Output:
	whether src and dst are equal
*/
bool Board::isEqualIndexes(string message)
{
	int src = 0, dst = 0;
	Helpers::messageToLocation(message, src, dst);
	return (src == dst);
}
/*
Input:
	the destnation and the source (like 'e4e2')
Output:
	whether src and dst are inside the borders of the board
*/
bool Board::isLegalIndex(string message)
{
	int src = 0, dst = 0;
	Helpers::messageToLocation(message, src, dst);

	// src and dst are lower than board size, and bigger or equal to 0
	return ((src < ARR_LEN && src >= 0) && (dst < ARR_LEN && dst >= 0));
}

/*
input:
	string with data about src and dst
output:
	whether we have soldier in this location
*/
bool Board::checkSoldierIn(char color, string message)
{
	int src = 0;
	char c = ' ';
	bool ret = false;

	Helpers::messageToLocation(message, src);
	c = this->_chars[src];

	if (color == '0')	// white
	{
		ret = isUpper(c);
	}
	else   // black
	{
		ret = isLower(c);
	}
	return ret;
}

/*
input:
	char
output:
	whether the char is an uppercase letter
*/
bool Board::isUpper(char c)
{
	bool ret = false;;
	if (c >= 'A' && c <= 'Z')
	{
		ret = true;
	}
	
	return ret;
}

/*
input:
	char
output:
	whether the char is a lowercase letter
*/
bool Board::isLower(char c)
{
	bool ret = false;
	if (c >= 'a' && c <= 'z')
	{
		return true;
	}
	
	return ret;
}