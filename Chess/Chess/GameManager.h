#ifndef GameManager_H
#define GameManager_H

#include "Pipe.h"
#include "Board.h"

class GameManager
{
public:
	GameManager(string whiteName, string blackName);
	~GameManager();
	void startGame();

private:
	void connectFrontend();

	Pipe _pipe;
	Board _board;
};

#endif