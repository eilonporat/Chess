#include "Player.h"

// ctor
Player::Player(bool isWhite, string name)
{
	int i = 0;
	this->_isWhite = isWhite;
	this->_name = name;
	
	//"rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1"

	// allocate memory for soldiers and init them
	if (isWhite)
	{
		this->_soldiers[0] = new Rook("a1", true);
		this->_soldiers[1] = new Knight("b1", true);
		this->_soldiers[2] = new Bishop("c1", true);
		this->_soldiers[3] = new King("d1", true);
		this->_soldiers[4] = new Queen("e1", true);
		this->_soldiers[5] = new Bishop("f1", true);
		this->_soldiers[6] = new Knight("g1", true);
		this->_soldiers[7] = new Rook("h1", true);
		this->_soldiers[8] = new Pawn("a2", true);
		this->_soldiers[9] = new Pawn("b2", true);
		this->_soldiers[10] = new Pawn("c2", true);
		this->_soldiers[11] = new Pawn("d2", true);
		this->_soldiers[12] = new Pawn("e2", true);
		this->_soldiers[13] = new Pawn("f2", true);
		this->_soldiers[14] = new Pawn("g2", true);
		this->_soldiers[15] = new Pawn("h2", true);
	}
	else
	{
		this->_soldiers[0] = new Rook("a8", false);
		this->_soldiers[1] = new Knight("b8", false);
		this->_soldiers[2] = new Bishop("c8", false);
		this->_soldiers[4] = new King("d8", false);
		this->_soldiers[3] = new Queen("e8", false);
		this->_soldiers[5] = new Bishop("f8", false);
		this->_soldiers[6] = new Knight("g8", false);
		this->_soldiers[7] = new Rook("h8", false);
		this->_soldiers[8] = new Pawn("a7", false);
		this->_soldiers[9] = new Pawn("b7", false);
		this->_soldiers[10] = new Pawn("c7", false);
		this->_soldiers[11] = new Pawn("d7", false);
		this->_soldiers[12] = new Pawn("e7", false);
		this->_soldiers[13] = new Pawn("f7", false);
		this->_soldiers[14] = new Pawn("g7", false);
		this->_soldiers[15] = new Pawn("h7", false);
	}
}

// dtor
Player::~Player()
{
	int i = 0;
	for (i = 0; i < SOLDIERS_NUM; i++)
	{
		delete this->_soldiers[i];
	}
}

/*
input:
	- char array representing the pieces on the board
output:
	whether there is a chess on the opponent
*/
bool Player::madeChess(char board[])
{
	int i = 0;
	bool isChess = false;
	int kingLocation = getKingLocation(board);	// find the location of the enemie's king
	for (i = 0; i < SOLDIERS_NUM; i++)
	{
		if (this->_soldiers[i] != NULL)
		{
			if (this->_soldiers[i]->isLegal(kingLocation,board))
			{
				isChess = true;
			}
		}
	}
	return isChess;

}

// NOTICE: CHECK THIS FUNCTION
/*
input:
	- char array representing the board
output:
	returns the location of the ENEMY king
*/
int Player::getKingLocation(char board[])
{
	char toSearch = 'K';
	int i = 0, ret = -1;
	
	// lowercase letter for black player - we check the ENEMY king
	if (this->_isWhite)
	{
		toSearch = 'k';	
	}

	// search the king
	for (i = 0; i < BOARD_SIZE; i++)
	{
		// if we found the king in the right color
		if (board[i] == toSearch)
		{
			// now ret is the location of the king
			ret = i;
		}
	}
	return ret;
}


/*
input:
	message got from server (like "e2e4")
	char array representing the board
output:
	whether the move is valid
ASSUMPTION: the indexes in the message are valid, not equal, and there is a player in the location the source pointing
			all of these are checked in the Board class before this function call
*/
bool Player::canMoveTo(string message, char board[])
{
	int src = 0, dst = 0, index = 0;
	bool ret = false;
	Helpers::messageToLocation(message, src, dst);

	// get index in "soldiers" array of current soldier (get the object)
	index = findSoldierAt(src);
	// checks if move is legal
	if (index != -1)
	{
		ret = this->_soldiers[index]->isLegal(dst, board);
	}
	
	return ret;
}

/*
input:
	location of a soldier on board
output:
	the index of the player in the "soldiers" array
*/
int Player::findSoldierAt(int src)
{
	int i = 0, location = 0, ret = -1;
	Soldier* curr = NULL;
	for (i = 0; i < SOLDIERS_NUM; i++)	
	{
		curr = this->_soldiers[i];
		if (curr != NULL)
		{
			// now location is the location of the current soldier
			Helpers::messageToLocation(curr->getLocation(), location);
			// checks if the current player has the same location as the argument - FOUND!
			if (location == src)
			{
				ret = i;	// return this index
			}
		}
	}

	return ret;
}

/*
input:
	src and dst - from where to start, where to go
output:
	soldier who is in the src location, now is in the dst location
*/
void Player::changeSoldierLocation(int src, int dst)
{
	// get the soldier object (get it's index in objects array)
	int index = this->findSoldierAt(src);
	// change the soldier location to the new location
	this->_soldiers[index]->moveTo(Helpers::locationToMessage(dst));
}

/*
input:
	receives the index of a soldier
output:
	delete it and free memory
*/
void Player::deleteSoldier(int index)
{
	int n = this->findSoldierAt(index);

	delete this->_soldiers[n];
	this->_soldiers[n] = NULL;
}

