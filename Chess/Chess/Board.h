#ifndef Board_H
#define Board_H

#define ARR_LEN 66		// length of char array
#define PLAYER_TURN 64	// this index in the char array representing the current player turn
#define ROW 8
#define COL 8
// codes that we send to the frontend
#define CODE_OK 0
#define MADE_CHESS_OK 1
#define NO_SRC 2
#define NO_DST 3
#define MADE_CHESS_ON_PLAYER 4
#define NON_BOARD_INDEXES 5
#define TOOL_CANT_MOVE 6 
#define DST_SRC_ARE_SAME 7


#include <iostream>
#include "Player.h"

class Board
{
public:
	Board(string whiteName, string blackName);
	~Board();

	char* getChars();
	void nextTurn();
	void makeMove(string msg);
	int getStatus(string msg);
	void printChars();

	bool checkSoldierIn(char color, string message);
	bool isLegalIndex(string message);
	bool isEqualIndexes(string message);


private:
	bool isUpper(char c);
	bool isLower(char c);

	bool dstHasTool(string msg, char turn);
	void rewind(string msg);

	char _chars[ARR_LEN];
	char _previousChars[ARR_LEN];
	Player _white;
	Player _black;
};

#endif