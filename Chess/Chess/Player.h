#ifndef Player_H
#define Player_H

#define SOLDIERS_NUM 16
#define BOARD_SIZE 64

#include "Soldier.h"

class Player
{
public:
	Player(bool isWhite, string name);
	~Player();
	bool madeChess(char board[]);
	bool canMoveTo(string message, char board[]);
	void changeSoldierLocation(int src, int dst);
	void deleteSoldier(int index);


private:
	int getKingLocation(char board[]);
	int findSoldierAt(int src);

	bool _isWhite;
	string _name;
	Soldier* _soldiers[SOLDIERS_NUM];
};

#endif