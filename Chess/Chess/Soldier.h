#ifndef Soldier_H
#define Soldier_H

#define BOARD_SIZE 64

#include <string>
#include "Helpers.h"

using namespace std;

class Soldier
{
public:
	Soldier(string location,bool isWhite);
	virtual ~Soldier();
	string getLocation();
	bool getIsWhite();
	virtual char getKind() = 0;
	void moveTo(string dst);
	virtual bool isLegal(int dst, char brd[]) = 0;

private:
	string _location;
	bool _isWhite;
};

class King : public Soldier
{
public:
	King(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};

class Rook : public Soldier
{
public:
	Rook(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};


class Bishop : public Soldier
{
public:
	Bishop(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};

class Pawn : public Soldier
{
private:
	bool _hasMoved;
public:
	Pawn(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};


class Queen : public Soldier
{
public:
	Queen(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};

class Knight : public Soldier
{
public:
	Knight(string location, bool isWhite);
	virtual bool isLegal(int dst, char brd[]);
	virtual char getKind();
};



#endif
