#ifndef Helpers_H
#define Board_H

#include <string>

class Helpers
{
public:
	static void messageToLocation(std::string message, int& src, int& dst);
	static void messageToLocation(std::string message, int& src);	// same as last function, just accepting only one location (only src)
	static std::string locationToMessage(int src);
};

#endif