#include "GameManager.h"

// ctor
GameManager::GameManager(string whiteName, string blackName) : _board(whiteName, blackName), _pipe()
{}

// dtor
GameManager::~GameManager()
{}

// function creates connection through the pipe to the frontent.
void GameManager::connectFrontend()
{
	bool isConnect = _pipe.connect();
	string ans;

	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = _pipe.connect();
		}
		else
		{
			_pipe.close();
			return;
		}
	}
	
}

// function handling the game
void GameManager::startGame()
{	
	char msgToGraphics[1024];
	int status = 0;
	
	// creates connection with graphics (using pipe)
	this->connectFrontend();

	// send the starting board
	strcpy_s(msgToGraphics, "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR1");
	_pipe.sendMessageToGraphics(msgToGraphics);   // send the board string
	// get message from graphics
	string msgFromGraphics = _pipe.getMessageFromGraphics();
	
	while (msgFromGraphics != "quit")
	{
		status = this->_board.getStatus(msgFromGraphics);
		// keep going to the next turn while the turns are valid
		if (status == CODE_OK || status == MADE_CHESS_OK)
		{
			this->_board.nextTurn();
		}
		else
		{
			cout << "invalid move: " << status << endl;
		}
		cout << "--------The Board---------" << endl;
		this->_board.printChars();
		// return result to graphics
		msgToGraphics[0] = status + '0';
		msgToGraphics[1] = 0;
		_pipe.sendMessageToGraphics(msgToGraphics);
		
		// get message from graphics
		msgFromGraphics = _pipe.getMessageFromGraphics();
	}
	system("PAUSE");
	_pipe.close();
	
}