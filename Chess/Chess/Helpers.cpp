#include "Helpers.h"

void Helpers::messageToLocation(std::string message, int& src, int& dst)
{
	// calculates src
	int col = message[0] - 'a';
	int row = 8 - (message[1] - '0');
	src = row * 8 + col;

	// calculates dst
	col = message[2] - 'a';
	row = 8 - (message[3] - '0');
	dst = row * 8 + col;
}

void Helpers::messageToLocation(std::string message, int& src)
{
	int col = message[0] - 'a';
	int row = 8 - (message[1] - '0');
	src = row * 8 + col;
}

std::string Helpers::locationToMessage(int src)
{
	std::string s = "";
	s += (src % 8) + 'a';		// col
	s += 8 - (src / 8) + '0';	// row
	return s;	
}
