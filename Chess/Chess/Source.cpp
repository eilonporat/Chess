#include <iostream>
#include "GameManager.h"

int main()
{
	srand(time_t(NULL));
	GameManager gm("WHITE", "BLACK");
	gm.startGame();

	return 0;
}