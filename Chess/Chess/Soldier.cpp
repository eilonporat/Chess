#include "Soldier.h"

//c'tor
Soldier::Soldier(string location, bool isWhite)
{
	this->_location = location;
	this->_isWhite = isWhite;
}
//d'tor
Soldier::~Soldier()
{
}
//getter
string Soldier::getLocation()
{
	return this->_location;
}
//getter
bool Soldier::getIsWhite()
{
	return this->_isWhite;
}
//setter
void Soldier::moveTo(string dst)
{
	this->_location = dst;
}

// checks if the rook movement is legal


King::King(string location, bool isWhite) : Soldier(location, isWhite)
{}

Rook::Rook(string location, bool isWhite) : Soldier(location, isWhite)
{}

Knight::Knight(string location, bool isWhite) : Soldier(location, isWhite)
{}

Pawn::Pawn(string location, bool isWhite) : Soldier(location, isWhite)
{
	this->_hasMoved = false;
}

Bishop::Bishop(string location, bool isWhite) : Soldier(location, isWhite)
{}

Queen::Queen(string location, bool isWhite) : Soldier(location, isWhite)
{}


//
char Rook::getKind()
{
	if (this->getIsWhite())
	{
		return 'R';
	}
	else
	{
		return 'r';
	}
}

bool Rook::isLegal(int dst, char brd[])
{
	int src = 0, i = 0;
	bool ret = false;
	Helpers::messageToLocation(this->getLocation(), src);

	// check if in the same row or col
	if (src / 8 == dst / 8)
	{
		ret = true;
		if (src < dst)
		{
			i = src + 1;
			while (i < dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i++;
			}
		}
		else
		{
			i = src - 1;
			while (i > dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i--;
			}
		}
	}
	else if (src % 8 == dst % 8)
	{
		ret = true;
		if (src < dst)
		{
			i = src + 8;

			while (i < dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i += 8;
			}
		}
		else
		{
			i = src - 8;

			while (i > dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i -= 8;
			}
		}
	}


	return ret;
}

// check if king movement is legal
bool King::isLegal(int dst, char brd[])
{
	int src = 0, valCol = 0, valRow = 0;
	Helpers::messageToLocation(this->getLocation(), src);

	valCol = (src % 8 - dst % 8);
	valRow = (src / 8 - dst / 8);
	if (valCol == 0 || valCol == 1 || valCol == -1)
	{
		if (valRow == 0 || valRow == 1 || valRow == -1)
		{
			return true;
		}
	}
	return false;
}

char King::getKind()
{
	if (this->getIsWhite())
	{
		return 'K';
	}
	else
	{
		return 'k';
	}
}

bool Knight::isLegal(int dst, char brd[])
{
	bool ret = false;
	int src = 0, srcX = 0 , srcY = 0, dstX = 0, dstY = 0;
	Helpers::messageToLocation(this->getLocation(), src);	// get src location

	srcX = src % 8; dstX = dst % 8; // X values of src and dst
	srcY = src / 8; dstY = dst / 8;	// Y values of src and dst

	if ((srcX - dstX == 2 || dstX - srcX == 2) && (srcY - dstY == 1 || dstY - srcY == 1))	// distance: x: 2, y: 1
	{
		ret = true;
	}
	else if ((srcX - dstX == 1 || dstX - srcX == 1) && (srcY - dstY == 2 || dstY - srcY == 2))	// distance: x-1, y-2
	{
		ret = true;
	}

	return ret;
}

char Knight::getKind()
{
	if (this->getIsWhite())
	{
		return 'N';
	}
	else
	{
		return 'n';
	}
}

bool Pawn::isLegal(int dst, char brd[])
{
	bool ret = false;
	int src = 0, srcX = 0, srcY = 0, dstX = 0, dstY = 0, direction = 1;
	Helpers::messageToLocation(this->getLocation(), src);	// get src location

	srcX = src % 8; dstX = dst % 8; // X values of src and dst
	srcY = src / 8; dstY = dst / 8;	// Y values of src and dst

	if (isupper(brd[src]))
	{
		direction = -1;
	}

	if (dstY - srcY == direction && srcX == dstX)// if moving forward 1 step
	{
		if (brd[dst] == '#')
		{
			ret = true;
		}
	}
	else if (dstY - srcY == 2 * direction && !this->_hasMoved && srcX == dstX)	// 2 steps and first turn
	{
		if (brd[dst] == '#' && brd[src + direction * 8] == '#')	// if way is free: 2 steps is free, 1 step is free
		{
			ret = true;
		}
	}
	else if ((dstY - srcY == direction) && (srcX - dstX == direction || dstX - srcX == direction))	// if eating: (y distance: 1, x distance: 1)
	{
		// if there is an enemy in the dst
		if (isupper(brd[src]))
		{
			ret = islower(brd[dst]);
		}
		else
		{
			ret = isupper(brd[dst]);
		}
	}

	if (ret)
	{
		this->_hasMoved = true;
	}

	return ret;
}

char Pawn::getKind()
{
	if (this->getIsWhite())
	{
		return 'P';
	}
	else
	{
		return 'p';
	}
}

bool Queen::isLegal(int dst, char brd[])
{
	int src = 0, count = 0, i = 0;
	bool ret = false;
	Helpers::messageToLocation(this->getLocation(), src);

	// check if in the same row or col
	if (src / 8 == dst / 8)
	{
		ret = true;
		if (src < dst)
		{
			i = src + 1;
			while (i < dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i++;
			}
		}
		else
		{
			i = src - 1;
			while (i > dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i--;
			}
		}
	}
	else if (src % 8 == dst % 8)
	{
		ret = true;
		if (src < dst)
		{
			i = src + 8;

			while (i < dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i += 8;
			}
		}
		else
		{
			i = src - 8;

			while (i > dst)
			{
				if (brd[i] != '#')
				{
					ret = false;
				}
				i -= 8;
			}
		}
	}

	if (src % 8 < dst % 8 && src / 8 > dst / 8)//top right
	{
		src -= 7;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src -= 7;
		}
		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 > dst % 8 && src / 8 > dst / 8)//top left
	{
		src -= 9;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src -= 9;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 < dst % 8 && src / 8 < dst / 8)//bot right
	{
		src += 9;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src += 9;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 > dst % 8 && src / 8 < dst / 8)//bot left
	{
		src += 7;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src += 7;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	return ret;
}

char Queen::getKind()
{
	if (this->getIsWhite())
	{
		return 'Q';
	}
	else
	{
		return 'q';
	}
}

bool Bishop::isLegal(int dst, char brd[])
{
	int src = 0, count = 0, i = 0;
	bool ret = false;
	Helpers::messageToLocation(this->getLocation(), src);


	if (src % 8 < dst % 8 && src / 8 > dst / 8)//top right
	{
		src -= 7;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src -= 7;
		}
		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 > dst % 8 && src / 8 > dst / 8)//top left
	{
		src -= 9;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src -= 9;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 < dst % 8 && src / 8 < dst / 8)//bot right
	{
		src += 9;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src += 9;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	else if (src % 8 > dst % 8 && src / 8 < dst / 8)//bot left
	{
		src += 7;
		while (src != dst && src < BOARD_SIZE && dst < BOARD_SIZE && src > 0 && dst > 0 && brd[src] == '#')
		{
			src += 7;
		}

		if (src == dst)
		{
			ret = true;
		}
	}
	return ret;
}

char Bishop::getKind()
{
	if (this->getIsWhite())
	{
		return 'B';
	}
	else
	{
		return 'b';
	}
}

